package ro.utcluj.isp.curs1;
import java.util.*;
public class HelloIsp {

    public static void main(String[] args) {
        System.out.println("Hello ISP!");

        Car c1 = new Car();
        System.out.println("Car speed is = "+ c1.speed);
        ArrayList<Car> list = new ArrayList<>();
        while(true){
            Car c2 = new Car();
            list.add(c2);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Car{
    int speed;
}
