package hulea.mihai.lab5.ex2;

public class ProxyImage implements Image{

    private Image image;
    private String fileName;

    public ProxyImage(String fileName, boolean t){
        this.fileName = fileName;
        if(t)
            image = new RealImage(fileName);
        else
            image = new RotatedImage(fileName);
    }

    @Override
    public void display() {
        image.display();
    }
}