package hulea.mihai.lab5.ex1;

public class OttoEngine16V extends Engine {

    @Override
    void sendECUCommand(String cmd) {
        System.out.println("Sending command "+cmd+" to ECU for Otto engine!");
    }
}
