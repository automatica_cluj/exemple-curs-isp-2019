package hulea.mihai.lab5.ex1;

public abstract class Engine {
    private boolean started;

    public void onOf(){
        started=!started;
        sendECUCommand("DO SOMETHING");

    }

    abstract void sendECUCommand(String cmd);

}
