package hulea.mihai.lab5.testSuper;


abstract class A{
    void f(){
        System.out.println("Message from A.");
    }

    abstract void g();
}

class B extends A{

    void f(){
        System.out.println("Message from B.");
    }

    @Override
    void g() {
        System.out.println("Implement g functionality here!");
    }


}


class C extends B {

    void f(){
        System.out.println("Message from C.");
        super.f();
    }
}

public class Test {
    public static void main(String[] args) {
        C ax = new C();
    }
}
