/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coronometru;

import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Cronometru extends Observable implements Runnable {
    
    Integer lock = new Integer(0);
    boolean pauza = false;
    int k = 0 ;
    
    public void run(){
        while(true){
            if(pauza){
                synchronized(this){
                    try {
                        wait();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Cronometru.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            k++;
            System.out.println("Counter is "+k);
            this.setChanged();
            this.notifyObservers(""+k);
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(Cronometru.class.getName()).log(Level.SEVERE, null, ex);
            }
      
        }
    }
    
    void pauzaCronometru(){
        pauza = true;
    }
    
    synchronized void startCronometru(){
        pauza = false;
       // synchronized(lock){
            notify();
       // }
    }
    
}
