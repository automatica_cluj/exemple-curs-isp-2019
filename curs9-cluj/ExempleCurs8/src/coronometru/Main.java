/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coronometru;

/**
 *
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        
        Cronometru c = new Cronometru();
        
        UICronometru ui = new UICronometru(c);
        ui.setVisible(true);
        
        c.addObserver(ui);
        
        Thread t = new Thread(c);
        t.start();
    }
}
