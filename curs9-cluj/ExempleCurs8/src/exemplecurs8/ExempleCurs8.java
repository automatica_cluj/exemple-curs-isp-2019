/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplecurs8;

import java.util.logging.Level;
import java.util.logging.Logger;

class Counter extends Thread{
    
    public void run(){
        while(true){
            System.out.println("Do something!");    
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(Counter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

public class ExempleCurs8 {

    public static void main(String[] args) {
         Counter c1 = new Counter();
         Counter c2 = new Counter();
         //c1.setDaemon(true);
         //c2.setDaemon(true);
         c1.start();
         c2.start();
    }
    
}
