/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.utcluj.isp.elevator;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mihai.hulea
 */
public class Controller extends Thread {
    
    private ArrayList<Job> jobs = new ArrayList<Job>();
    private Job currentJob;
    private Elevator e;
    
    Controller(Elevator e){
        this.e = e;
    }
    
    public void run(){
        
        while(true){
           if(currentJob == null && jobs.size()>0){
               currentJob = jobs.remove(0);
           }
           
           if(currentJob!=null){
              if(currentJob.getDestinationFloor()*10 > e.getPosition()){
                 e.move(Direction.UP);
                  System.out.println("Command UP");
              }else if(currentJob.getDestinationFloor()*10 < e.getPosition()) {
                 e.move(Direction.DOWN);
                 System.out.println("Command DOWN");
              }else{
                  currentJob = null;
                  e.move(Direction.HOLD);
                  System.out.println("Command HOLD");
              }
           }
           
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
           
           
        }
        
    }
    
    public void addJob(Job next){
        jobs.add(next);
    }
    
    
    
}

class Job{
    private int destinationFloor;

    public Job(int destinationFloor) {
        this.destinationFloor = destinationFloor;
    }

    public int getDestinationFloor() {
        return destinationFloor;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Job other = (Job) obj;
        if (this.destinationFloor != other.destinationFloor) {
            return false;
        }
        return true;
    }
    
    
    
}
