
package ro.utcluj.isp.elevator;


public class Elevator2019 {
    
    
    public void testElevator() throws InterruptedException{
            Elevator e1 = new Elevator();
           Thread t = new Thread(e1);
           
           t.start();
           
           Thread.sleep(3000);
           
           e1.move(Direction.UP);
           
           
           Thread.sleep(2000);
           
           e1.move(Direction.DOWN); 
    }
  
    public static void main(String[] args) throws InterruptedException {
           Elevator e1 = new Elevator();
           Thread t = new Thread(e1);
           
           Controller ctrl = new Controller(e1);
           
           ctrl.addJob(new Job(3));
           ctrl.addJob(new Job(1));
           ctrl.addJob(new Job(2));
           
           t.start();
           ctrl.start();
                   
           
           
           
    }
    
}
