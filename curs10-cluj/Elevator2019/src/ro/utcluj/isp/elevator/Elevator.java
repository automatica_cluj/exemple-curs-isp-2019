
package ro.utcluj.isp.elevator;

import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Elevator extends Observable implements Runnable{
    private boolean active = true;
    
    private Direction currentDirection = Direction.HOLD;
    private int position = 0; 
    
    @Override
    public void run() {
        while(active){
            if(currentDirection==Direction.UP){
                position++;
                System.out.println("Moving UP "+position);
                setChanged();
                notifyObservers(new Integer(position));
            }else if(currentDirection==Direction.DOWN){
                position--;
                System.out.println("Moving DOWN "+position);        
                setChanged();
                notifyObservers(new Integer(position));
            }else{
                System.out.println("Hold "+position);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Elevator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int getPosition() {
        return position;
    }
    
    public void move(Direction d){
        this.currentDirection = d;
    }
}

enum Direction{
    UP, DOWN, HOLD;
}
