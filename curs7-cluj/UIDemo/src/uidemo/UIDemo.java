
package uidemo;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class UIDemo {

     public static void main(String[] args) {
        // TODO code application logic here
        CounterUI ui1 = new CounterUI();
    }
    
}


class CounterUI extends JFrame implements ActionListener{
    int k;
    JButton bClick;
    JTextField tfCounter;
    
    CounterUI(){
        setTitle("Counter in Swing!");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300,400);
        
        bClick = new JButton("Click me!");
        tfCounter = new JTextField();
        getContentPane().setLayout(new FlowLayout());
        getContentPane().add(bClick);
        getContentPane().add(tfCounter);
        tfCounter.setText("00000");
        
        bClick.addActionListener(this);
        
        //clasa interna anonima
        bClick.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                System.out.println("Click 22222!!!");
            }
        });
        
        bClick.addActionListener(new OtherActionListener(this));
        
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        k++;
        System.out.println("CLICK!!!");
        this.tfCounter.setText(""+k);
    }
    
 
}//.class

class OtherActionListener implements ActionListener{
    
    CounterUI gui;

    public OtherActionListener(CounterUI gui) {
        this.gui = gui;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        gui.k++;
        System.out.println("CLICK!!!");
        gui.tfCounter.setText(""+gui.k);
    }
}//.class