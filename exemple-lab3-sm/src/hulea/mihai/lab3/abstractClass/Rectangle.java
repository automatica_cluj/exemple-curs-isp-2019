package hulea.mihai.lab3.abstractClass;

public class Rectangle extends Shape {
    private int width;
    private int height;

    public Rectangle(String color, boolean filled, int width, int height) {
        super(color, filled);
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw() {
        System.out.println("Drawing rectangle "+width+" "+height);
    }
}
