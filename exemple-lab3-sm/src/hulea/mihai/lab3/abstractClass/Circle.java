package hulea.mihai.lab3.abstractClass;

public class Circle extends Shape {
    private int radius;
    private int x;
    private int y;

    public Circle(String color, boolean filled, int radius, int x, int y) {
        super(color, filled);
        this.radius = radius;
        this.x = x;
        this.y = y;
    }

    @Override
    public void draw() {
        System.out.println("Drawing circle "+x+" "+y+" radius= "+radius);
    }
}
