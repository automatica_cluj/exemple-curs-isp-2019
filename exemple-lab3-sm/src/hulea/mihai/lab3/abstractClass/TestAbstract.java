package hulea.mihai.lab3.abstractClass;

public class TestAbstract {
    public static void main(String[] args) {
//        Shape s1 = new Shape("red", false);
//        s1.draw();

        Circle c1 = new Circle("blue",true, 5, 8, 10);
        c1.draw();
        Shape c2 = new Circle("red",false, 5, 10, 10);
        c2.draw();

        // [null, null, null, null, null, null, null, null, null, null]
        Shape[] list = new Shape[10];
        list[0] = c1;
        list[1] = c2;
        list[3] = new Rectangle("yellow", false, 6, 8);
        // [87y8y, g7f7f7, 546fd5d5, null, null, null, null, null, null, null]
        for(Shape x: list){
          //  if(x!=null)
                x.draw();
        }
    }
}
