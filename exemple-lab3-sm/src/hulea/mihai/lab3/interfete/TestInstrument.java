package hulea.mihai.lab3.interfete;

public class TestInstrument {
    public static void main(String[] args) {
        Instrument i1 = new Pian();
        Instrument i2 = new Vioara();

        i1.afieaza();
        i2.canta();

        i1.afieaza();
        i2.canta();
    }
}
