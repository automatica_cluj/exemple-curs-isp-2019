package hulea.mihai.lab3.interfete;

public interface Instrument {

    void canta();
    void afieaza();
}


class Pian implements Instrument{
    @Override
    public void canta() {
        System.out.println("Pianul canta");
    }

    @Override
    public void afieaza() {
        System.out.println("Un pian");
    }
}

class Vioara implements Instrument{
    @Override
    public void canta() {
        System.out.println("Vioara canta");
    }

    @Override
    public void afieaza() {
        System.out.println("O vioara");
    }
}
