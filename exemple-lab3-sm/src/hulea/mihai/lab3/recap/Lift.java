package hulea.mihai.lab3.recap;

public class Lift {
    public static final int ETAJ_MAX = 10;
    private int etajCurent;
    private boolean usaInchisa;

    public Lift(int etajCurent, boolean usaInchisa) {
        this.etajCurent = etajCurent;
        this.usaInchisa = usaInchisa;
    }

    public Lift() {
        this.usaInchisa = true;
        this.etajCurent = 0;
    }

    void comanda(int etaj){
        if(etaj>ETAJ_MAX || etaj < 0){
            System.out.println("Comanda incorecta!");
            return;
        }
        if(etaj!=etajCurent){
            int k = etaj - etajCurent;
            if(k > 0){
                while(etajCurent!=etaj){
                    etajCurent++;
                    System.out.println("Liftul urca la "+etajCurent);
                    try{Thread.sleep(1000);}catch(Exception e){}
                }
            }else{
                while(etajCurent!=etaj){
                    etajCurent--;
                    System.out.println("Liftul coboara la "+etajCurent);
                    try{Thread.sleep(1000);}catch(Exception e){}
                }
            }
        }else{
            System.out.println("Liftul la etajul cerut "+etajCurent);
        }
        try{Thread.sleep(1000);}catch(Exception e){}
        System.out.println("Se deschid usile!");
        this.usaInchisa = false;
        try{Thread.sleep(1000);}catch(Exception e){}
        System.out.println("Se inchid usile!");
        this.usaInchisa = false;
    }

    @Override
    public String toString() {
        return "Lift{" +
                "etajCurent=" + etajCurent +
                ", usaInchisa=" + usaInchisa +
                '}';
    }
}
