package hulea.mihai.lab3.recap;

public class TestLift {
    public static void main(String[] args) {
        Lift l1 = new Lift();

        Lift l2 = new Lift(3, true);

        l1.comanda(-1);
        l1.comanda(4);
        l1.comanda(0);

        l2.comanda(7);

        System.out.println(l1);
        System.out.println(l2);
    }
}
