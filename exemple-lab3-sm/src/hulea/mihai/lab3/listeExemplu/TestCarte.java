package hulea.mihai.lab3.listeExemplu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TestCarte {
    public static void main(String[] args) {
        ArrayList<Carte> lista = new ArrayList<>();
        lista.add(new Carte("A B", 190));
        lista.add(new Carte("X Y", 78));

        for(Carte c: lista)
            System.out.println(c);

        Carte c1 = new Carte("T U", 100);
        Carte c2 = new Carte("T U", 100);
        if(c1 == c2){
            System.out.println("Carti identice.");
        }else{
            System.out.println("Carti diferite.");
        }

        if(c1.equals(c2)){
            System.out.println("Carti identice.");
        }else{
            System.out.println("Carti diferite.");
        }

        ComparatorPagin cp = new ComparatorPagin();
        Collections.sort(lista, cp);

        //clase interne anonime
        Collections.sort(lista,
                new Comparator<Carte>(){
                    @Override
                    public int compare(Carte o1, Carte o2) {
                        return o1.nrPagini - o2.nrPagini;
                    }
                }
                );

    }
}

class ComparatorPagin implements Comparator<Carte>{

    @Override
    public int compare(Carte o1, Carte o2) {
        return o1.nrPagini - o2.nrPagini;
    }
}