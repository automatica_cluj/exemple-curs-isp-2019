package hulea.mihai.lab3.listeExemplu;

import java.util.Objects;

public class Carte {
    private String autor;
    public int nrPagini;

    public Carte(String autor, int nrPagini) {
        this.autor = autor;
        this.nrPagini = nrPagini;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Carte carte = (Carte) o;
//        return nrPagini == carte.nrPagini &&
//                Objects.equals(autor, carte.autor);
//    }

    public boolean equals(Object o) {
        Carte carte = (Carte) o;

       return nrPagini == carte.nrPagini &&
                autor.equals(carte.autor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(autor, nrPagini);
    }

    @Override
    public String toString() {
        return "Carte{" +
                "autor='" + autor + '\'' +
                ", nrPagini=" + nrPagini +
                '}';
    }
}
