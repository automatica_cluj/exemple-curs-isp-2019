/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testjunit;

public class Counter {

    private int k = 0;

    public void inc() {
        k++;
    }

    public void setValue(int k) {
        this.k = k;
    }

    public int getValue() {
        return k;
    }
}
