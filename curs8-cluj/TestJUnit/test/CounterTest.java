/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import testjunit.Counter;


public class CounterTest {

    Counter c;

    @Before
    public void init(){
        c = new Counter();
    }
    
    @Test
    public void incTest(){
        c.inc();
        c.inc();
        assertEquals(c.getValue(),2);
    }
    
    @Test
    public void incTest2(){
        c.inc();
        c.inc();
        assertEquals(c.getValue(),2);
    }
    
      @Test
    public void incTest3(){
        c.inc();
        c.inc();
        assertEquals(c.getValue(),2);
    }



    
}
