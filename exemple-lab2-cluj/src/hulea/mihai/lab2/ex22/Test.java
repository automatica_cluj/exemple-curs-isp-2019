package hulea.mihai.lab2.ex22;


/**
 * Exemplu de structurare a rezolvarilor pentru laboratorul 2. Se pot utiliza metode statice.
 */
public class Test {

    static void solutia1(){
        System.out.println("Varianta A de rezolvare");

    }

    static void solutia2(){
        System.out.println("varianta B de rezolvare");

    }

    public static void main(String[] args) {
        System.out.println("Hi!");
        solutia1();
        solutia1();
    }
}
