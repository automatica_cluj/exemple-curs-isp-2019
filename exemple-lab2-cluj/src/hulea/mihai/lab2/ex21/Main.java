package hulea.mihai.lab2.ex21;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static int k = 10;

    static int getRandom(){
        Random r = new Random();
        int i = r.nextInt(100);
        return i;
    }

    static void displayDelayedMessage(){
        System.out.println("Start timer...");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Hello!");
    }

    static void testReadFromConsole(){
        Scanner in = new Scanner(System.in);
        System.out.println("Introduceti cei 2 operanzi:");
        int x = in.nextInt();
        int y = in.nextInt();
        System.out.println("X+Y="+(x+y));
    }

    static int[] generateRandomArray(int size){
        int[] a = new int[size];
        for(int i=0;i<a.length;i++)
            a[i] = getRandom();

        return a;
    }


    public static void main(String[] args) {
	    int k = getRandom();
	    testReadFromConsole();

	    int[] b = generateRandomArray(15);

	    for(int i: b)
            System.out.println(i);

    }
}

