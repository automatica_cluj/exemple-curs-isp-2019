package hulea.mihai.curs5.liste;

public class Test {
    public static void main(String[] args) {
//        BankAccount a1 = new BankAccount("M",1);
//        BankAccount a2 = new BankAccount("M",1);
//
//        if(a1.equals(a2)){
//            //...
//        }else{
//            //.....
//        }

        AccountsManager m = new AccountsManager();
        m.addAccount("A",19);
        m.addAccount("B",8);
        m.addAccount("C",54);
        m.addAccount("D",9);

        m.listAccounts();

        m.transfer2("B", "C", 100);

        m.listAccounts();

    }
}
