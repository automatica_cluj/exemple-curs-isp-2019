package hulea.mihai.curs5.liste;
import java.util.*;

public class AccountsManager {

    private ArrayList<BankAccount> accounts =
            new ArrayList<>();

    void addAccount(String name, int balance) {
        accounts.add(new BankAccount(name, balance));
    }

    void addAccount(BankAccount a){
        accounts.add(a);
    }

    void transfer(String sender, String receiver, int amount){
        BankAccount s = null;
        BankAccount r = null;
        for(int i=0;i < accounts.size();i++){
            if(accounts.get(i).getName().equals(sender))
                s = accounts.get(i);
            if(accounts.get(i).getName().equals(receiver))
                r = accounts.get(i);
        }

        if(s!=null && r!=null){
            s.setBalance(s.getBalance()-amount);
            r.setBalance(r.getBalance()+amount);
        }
    }

    void transfer2(String sender, String receiver, int amount){
        int si = accounts.indexOf(new BankAccount(sender,0));
        int ri = accounts.indexOf(new BankAccount(receiver,0));

        BankAccount r = accounts.get(si);
        BankAccount s = accounts.get(ri);


        if(s!=null && r!=null){
            s.setBalance(s.getBalance()-amount);
            r.setBalance(r.getBalance()+amount);
        }
    }

    void listAccounts(){

        /// for each
//        for(BankAccount a: accounts)
//            System.out.println(a);
//
//        //for clasic
//        for(int i=0; i<accounts.size();i++){
//            BankAccount a = accounts.get(i);
//            System.out.println(a);
//        }

        //iterator
        Iterator<BankAccount> i = accounts.iterator();
        while(i.hasNext()){
            BankAccount a = i.next();
            System.out.println(a);
        }
    }




}
