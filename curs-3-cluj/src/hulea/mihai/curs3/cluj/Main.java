package hulea.mihai.curs3.cluj;

class Instrument{
    void play(){
        System.out.println("Instrument play!");
    }
}

class Pian extends Instrument{

    @Override
    void play(){
        System.out.println("Pian play!");
    }
}

final class Pianina extends Pian{
    @Override
    void play(){
        System.out.println("Pianina play!");
    }
}
class Orchestra2{
    Instrument[] list = new Instrument[10];

    Orchestra2(){
        list[0] = new Pian();
        list[1] = new Pianina();
    }

    void playAll(){
        for(Instrument i: list)
            if(i!=null)
                i.play();
    }
}

class Orchestra{
    Pian[] list1 = new Pian[10];
    Pianina[] list2 = new Pianina[10];

    Orchestra(){
        list1[0] = new Pian();
        list2[0] = new Pianina();
    }

    void playAll(){
        for(Pian x: list1)
            if(x!=null)
                x.play();

        for(Pianina x: list2)
            if(x!=null)
                x.play();

    }

}

public class Main {

    public static void main(String[] args) {
//	    Pian p1 = new Pian();
//	    p1.play();
//
//	    Instrument i1 = new Pian();
//	    i1.play();
//
//	    Instrument i2 = new Instrument();
//	    i2.play();
//
//	  //  Pian p2 = new Instument(); //eroare de compilare
//        Pian p = new Pianina();
//
//        Pianina pi1 = new Pianina();
//        pi1.play();

        Orchestra o = new Orchestra();
        o.playAll();

        Orchestra2 o2 = new Orchestra2();
        o2.playAll();


    }
}
