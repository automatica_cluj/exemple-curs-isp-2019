package hulea.mihai.curs3.cluj;

import java.util.Objects;

public class ComparareObiecte {
    public static void main(String[] args) {
        Person p1 = new Person("A",2);
        Person p2 = new Person("A",2);
        Person p3 = p1;
        if(p1 == p2){
            System.out.println("Obiecte egale!"); //gresit
        }

        if(p1 == p3){
            System.out.println("Obiecte egale!"); //gresit
        }

        Object x = new Person("X",5);
        ((Person) x).calculateIncome();
        //x.calculateIncome(); //nu este vizibil

        if(p1.equals(p2)){
            System.out.println("Obiectele sunt egale.");
        }

    }
}


class Person{
    private int age;
    private String name;

    public Person(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public void calculateIncome(){
        System.out.println("XXXXXXXXXXXXXXXXXXXXXxxx");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                name.equals(person.name);
    }

}