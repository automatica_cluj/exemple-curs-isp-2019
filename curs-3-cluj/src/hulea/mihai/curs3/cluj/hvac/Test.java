package hulea.mihai.curs3.cluj.hvac;

import java.util.Random;

class Senzor{
    private int value;

    public void displaySenzorValue(){
        System.out.println("Senzor value is "+value);
    }
    public Senzor(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

class SenzorTemperatura extends Senzor{

    public SenzorTemperatura(int value) {
        super(value);
    }

    @Override
    public void displaySenzorValue() {
        System.out.println("Temperature is "+getValue());
    }
}

class SenzorUmiditate extends Senzor{

    public SenzorUmiditate(int value) {
        super(value);
    }

    @Override
    public void displaySenzorValue() {
        super.displaySenzorValue();
    }
}

class HVAC {
    boolean incalzire;
    boolean umiditate;

    public void actioneazaIncalizre(){
        incalzire = !incalzire;
        if(incalzire)
            System.out.println("Incalizire PORNITA!");
        else
            System.out.println("Incalizire OPRITA!");
    }


    public void actioneazaDezumidifcator(){
        incalzire = !incalzire;
        if(incalzire)
            System.out.println("Dezumidificare PORNITA!");
        else
            System.out.println("Dezumidificare OPRITA!");
    }
}

class Controler{
    private SenzorTemperatura t1;
    private SenzorUmiditate t2;
    private HVAC hvac;

    public Controler(SenzorTemperatura t1, SenzorUmiditate t2, HVAC hvac) {
        this.t1 = t1;
        this.t2 = t2;
        this.hvac = hvac;
    }

    public void control(){
        if(t1.getValue()>10&&t2.getValue()<50){
            hvac.actioneazaIncalizre();
        }else{
            hvac.actioneazaDezumidifcator();
        }

    }
}

public class Test {

    public static void main(String[] args) throws InterruptedException {
        SenzorUmiditate s1 = new SenzorUmiditate(1);
        SenzorTemperatura s2 = new SenzorTemperatura(2);
        HVAC h = new HVAC();
        Controler c1 = new Controler(s2, s1, h);

        Random r = new Random();
        while(true){

            s1.setValue(r.nextInt(70));
            s2.setValue(r.nextInt(70));
            c1.control();
            Thread.sleep(1000);
        }


    }
}
