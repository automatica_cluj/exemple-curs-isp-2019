package hulea.mihai.lab3.cluj.exemplul4;

/**
 * Punct de plecare rezolvare exercitiul 4, ultimul punct.
 */
public class MyPoint {
    public double distance(int x, int y){
        // ....
        return 0;
    }

    public double distance(MyPoint other){
        //compute here distance from 'this' to 'other'
        // x ... other.x  y ... other.y
        return 0;
    }
}
