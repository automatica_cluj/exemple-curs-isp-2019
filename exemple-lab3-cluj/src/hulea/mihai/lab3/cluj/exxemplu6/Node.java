package hulea.mihai.lab3.cluj.exxemplu6;

public class Node {
    private Node next;
    private int value;

    public Node(int value) {
        this.value = value;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public static void main(String[] args) {
        Node head = new Node(10);
        Node n1 = new Node(9);
        head.setNext(n1);
        Node n2 = new Node(7);
        n1.setNext(n2);
        System.gc();
    }
}
