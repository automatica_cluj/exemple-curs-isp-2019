package hulea.mihai.lab3.cluj.exemplul1;

public class TestSenzor {

    public static void main(String[] args) {
        Senzor s1 = new Senzor(10);
        System.out.println(s1);
        s1.setValue(199);
        System.out.println(s1);
    }
}
