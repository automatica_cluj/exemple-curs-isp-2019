package hulea.mihai.lab3.cluj.exemplul1;

public class Senzor {
    private int value;

    public Senzor(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Senzor{" +
                "value=" + value +
                '}';
    }
}
