package hulea.mihai.lab3.cluj.exemplul2;

public class Car2 {
    private String color;
    private int doors;

    public Car2(String color, int doors) {
        this.color = color;
        this.doors = doors;
    }

    public Car2(){
        this.color = "Black";
        this.doors = 4;
    }
}
