package hulea.mihai.lab3.cluj.exemplul2;

public class Car {
    private String color;

    /**
     * Default contructor / contructor implicit.
     */
    public Car() {
        color = "Black";
    }

    public Car(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Car c1 = new Car();
        Car c2 = new Car("Red");
    }
}
