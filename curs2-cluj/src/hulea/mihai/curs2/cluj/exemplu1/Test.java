package hulea.mihai.curs2.cluj.exemplu1;

public class Test {

    static void showRobot(Robot r){
        System.out.println(r);
    }

    public static void main(String[] args) {
        Robot r1 = new Robot();
        Robot r2 = new Robot();
        Robot r3;
        r3 = new Robot();

        Robot r4 = null;

        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);
        System.out.println(r4);

        r1 = r2;
        System.out.println(".........");
        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);
        System.out.println(r4);

        System.out.println(r1.x);
        r1.x = 10;
        r2.x = 99;
        System.out.println(">>>r1 "+r1.x);
        System.out.println(">>>r2 "+r2.x);

        System.out.println("........");
        r3.x = 888;
        System.out.println(">>>r1 "+r1.x);
        System.out.println(">>>r2 "+r2.x);
        System.out.println(">>>r2 "+r3.x);
    }
}
