package hulea.mihai.curs2.cluj.exemplu2;

public class Robot {

    private int speed;
    private int x;
    private int y;
    private String name;

//    public void printLocation(){
//
//    }


    public int getX() {
        return x;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getY() {
        return y;
    }

    public void move(){
        x+=speed;
        y+=speed;
    }

    public void move(int d){
        for(int i=0;i<=d;i++)
            move();
    }

    @Override
    public String toString() {
        return "Robot{" +
                "speed=" + speed +
                ", x=" + x +
                ", y=" + y +
                ", name='" + name + '\'' +
                '}';
    }
}
