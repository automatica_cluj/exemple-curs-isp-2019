package hulea.mihai.curs2.cluj.exemplu3;

public class Robot {

    private int speed;
    private int x;
    private int y;
    private String name;

    public Robot() {
        this(2,2,4,"DefaultRobot");
    }

    public Robot(String name) {
        this(1,1,1,name);
    }

    public Robot(int speed, int x, int y, String name) {
        this.speed = speed;
        this.x = x;
        this.y = y;
        this.name = name;
    }

    public int getX() {
        return x;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getY() {
        return y;
    }

    public void move(){
        x+=speed;
        y+=speed;
    }

    public void move(int d){
        for(int i=0;i<=d;i++)
            move();
    }

    @Override
    public String toString() {
        return "Robot{" +
                "speed=" + speed +
                ", x=" + x +
                ", y=" + y +
                ", name='" + name + '\'' +
                '}';
    }
}
