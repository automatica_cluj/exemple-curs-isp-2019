Sa se modifice aplicatia de la punctul 5 pentru a utiliza mecanismele Java de tratare a erorilor (exceptiile) pentru a
gestiona urmatoarele situatii:
 - introducerii unui cod gresit;
 - depasirii numarului de incercari succesive nereusite;