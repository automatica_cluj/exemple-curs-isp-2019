package hulea.mihai.lab4.sm.ex3;

import hulea.mihai.lab4.sm.ex2.AccessKey;

import java.util.ArrayList;

public class AccessKeyManager {

    private ArrayList<AccessKey> keys = new ArrayList<>();

    void addNewKey(String accessCode, String userName){
        //sa se implementeze secventa de cod pentru adaugarea unei noi chei in colectia keys
    }

    boolean hasAccess(String accessCode){
        //sa se implementeze secventa de cod care returneaza true daca
        //codul de access exista in una dintre cheile salvate in colectia 'keys'
        //si false in caz contrar
        return false;
    }

    void removeKey(String userName){
        //sa se stearga din vectorul keys acea cheie care are numele
        //identic cu numele 'userName' primit ca si argument in aceasta metoda
    }

    void displayAll(){
        //sa se afieze toate cheile de access si numele asociate cu acestea
    }

    public static void main(String[] args) {
        //sa se constriasca un obiect de tip AccessKeyManager sa se testeze
        //metodele definite
    }
}
