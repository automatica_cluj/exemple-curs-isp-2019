package hulea.mihai.lab4.sm.ex5;

import java.util.ArrayList;

public class DoorControler {

    ArrayList<AccessKey> accessKeys = new ArrayList<>();
    ArrayList<LockEvent> lockEvents = new ArrayList<>();


    DoorControler(){
        //sa se adauge in accessKeys 5 chei de access
    }

    void receiveAccessKey(AccessKey accessKey){
        //sa se verifice daca cheia de access este identica cu una din cheile preznte in accessKeys
        //daca cheia de access exista sa se apeleze door unlock
        //daca cheia nu exista sa se trimta un sms si se stocheaza in log events un obiect de tip LockEvent
        //daca sa-au efectuat 3 incercari succesive nereusite de unlock atunci functia de lock se
        //blocheaza permanent pana la apelare metodei reset()
    }

    void viewAllLockEvents(){
        //sa se afiseze toate evenimentele stocate in lockEvents
    }

    void rest(){
        //...
    }

    public static void main(String[] args) {
        //sa se testeze clasa DoorControler
    }

}
