
package hulea.mihai.exempluControlerTemp1;


public class SenzorTemperatura extends Senzor {

    public SenzorTemperatura(int value) {
        super(value);
    }

    @Override
    public void displaySenzorInfo() {
        System.out.println("Temperature senzor value is "+getValue()+" C");
    }
    
    
}
