
package hulea.mihai.exempluControlerTemp1;

import java.util.Random;


public class TestControler {

    public static void main(String[] args) throws InterruptedException {
        Random r = new Random();
        
        SenzorTemperatura t = new SenzorTemperatura(0);
        SenzorUmiditate u = new SenzorUmiditate(0);
        HVAC hvac = new HVAC();
        
        Controler ctrl = new Controler(t, u, hvac);
        int k = 0;
        while(k<100){
            k++;
            t.setValue(r.nextInt(10));
            u.setValue(r.nextInt(10));
            ctrl.control();
            System.out.println("### "+k);
            Thread.sleep(1000);
        }
        
    }
}
