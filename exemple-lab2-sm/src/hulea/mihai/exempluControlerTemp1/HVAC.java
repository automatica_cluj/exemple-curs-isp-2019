
package hulea.mihai.exempluControlerTemp1;


public class HVAC {
   private boolean ac;
   private boolean dehum;
   
   public void switchAC(){
       ac = !ac;
       if(ac)
           System.out.println("AC is turned ON!");
       else
           System.out.println("AC is turned OFF!");
   }
   
   public void switchDezhum(){
       dehum = !dehum;
       if(dehum)
           System.out.println("Dehumidifier is turned ON!");
       else
           System.out.println("Dehumidifier is turned OFF!");
   }
}
