
package hulea.mihai.exempluControlerTemp1;

public class Controler {
    private SenzorTemperatura t;
    private SenzorUmiditate u;
    private HVAC hvac;

//    public Controler() {
//        t = new SenzorTemperatura(0);
//        u = new SenzorUmiditate(0);
//        hvac = new HVAC();
//    }

    public Controler(SenzorTemperatura t, SenzorUmiditate u, HVAC hvac) {
        this.t = t;
        this.u = u;
        this.hvac = hvac;
    }
  
    public void control(){
        System.out.println("Execute control command for: ");
        t.displaySenzorInfo();
        u.displaySenzorInfo();
        if(t.getValue() <=5 || u.getValue() >5){
            
            hvac.switchAC();
        }else{
            hvac.switchDezhum();
        }
            
    }
    
}
