
package hulea.mihai.exempluControlerTemp1;

public class Senzor {
    private int value;

    public Senzor(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
    
    public void displaySenzorInfo(){
        System.out.println("Senzor value is "+this.value);
    }
}
