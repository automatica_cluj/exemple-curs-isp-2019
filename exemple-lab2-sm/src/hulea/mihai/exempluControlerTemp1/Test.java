/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hulea.mihai.exempluControlerTemp1;

import java.util.Random;

/**
 *
 * @author mihai.hulea
 */
public class Test {
    
    //comportamentul polimorfic
    static void testPolimorifsm(){
        
        Random r = new Random();
        Senzor[] list = new Senzor[10];
        for(int i=0;i<10;i++){
            int k = r.nextInt(10); // nr in intervalul 0 - 9 
            if(k<4)
                list[i] = new SenzorTemperatura(k);
            else
                list[i] = new SenzorUmiditate(k);
        }
        
        for(Senzor s: list)
            s.displaySenzorInfo();
        
    }
    

    
    public static void main(String[] args) {
//        SenzorTemperatura t1 = new SenzorTemperatura(10);
//        t1.displaySenzorInfo();
//        
//        SenzorUmiditate u1 = new SenzorUmiditate(80);
//        u1.displaySenzorInfo();
//        
//        Senzor s1 = new SenzorTemperatura(56);
//        Senzor s2 = new SenzorUmiditate(78);
//        
//        s1.displaySenzorInfo();
//        s2.displaySenzorInfo();
//        
//        System.out.println("..............");
        
        testPolimorifsm();
//        System.out.println("..............");
//        HVAC h1 = new HVAC();
//        h1.switchAC();
//        h1.switchAC();
//        h1.switchDezhum();
//        h1.switchDezhum();
    }
}
