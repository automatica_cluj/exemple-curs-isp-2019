
package hulea.mihai.lab2.ex1;

import java.util.Scanner;

public class MaxNumber {
    
    static void afiseazaMaxim(int a, int b){
        if(a>=b){
            System.out.println("MAX = "+a);
        }else{
            System.out.println("MAX = "+b);
        }
    }
    
    public static void main(String[] args) {
        System.out.println("Afisare maxim!");
        Scanner s = new Scanner(System.in);
        System.out.println("Introduceti primul numar:");
        int x = s.nextInt();
        System.out.println("Introduceti al doilea numar:");
        int y = s.nextInt();
        afiseazaMaxim(y, y);
    }
}
