package hulea.mihai.lab3.ex2;


public class Circle {
    private double radius;
    private String color;
    
    public Circle(){ //no-args contructor
        radius = 1.0; 
        color = "red";
    }
    
    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() { //.getter
        return radius;
    }
    
 
    public void setRadius(double radius) { //.setter
        this.radius = radius;
    }
    
    

    double getArea(){
        return radius * Math.PI;
    }
    
}
