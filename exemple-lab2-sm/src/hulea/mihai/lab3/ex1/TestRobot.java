
package hulea.mihai.lab3.ex1;


public class TestRobot {
    public static void main(String[] args) {
        Robot r1 = new Robot();
        Robot r2 = new Robot();
        
        r1.change(9);
        r2.change(3);
        
        System.out.println(r1);
        System.out.println(r2.toString());
        
        Robot r3 = r1;
        
        System.out.println(r3);
    }
}
