
package hulea.mihai.lab4.ex4;

public class MyPoint {
    private int x;
    private int y;
    
    MyPoint(){
       this.x = 0;
       this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "MyPoint{" + "x=" + x + ", y=" + y + '}';
    }
    
    
    
}

