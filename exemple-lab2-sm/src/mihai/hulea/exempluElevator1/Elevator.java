/**
 * Exemplu setter getter
 */
package mihai.hulea.exempluElevator1;

public class Elevator {
    private int floor;

    public Elevator(int floor) {
        this.floor = floor;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }
    
      
    public static void main(String[] args) {
        Elevator e = new Elevator(0);
        e.setFloor(7);
        
        System.out.println("Elevator is at floow "+e.getFloor());
        
    }
}
